# absent

## Project setup
Make sure you [install yarn](https://classic.yarnpkg.com/en/docs/install/) before you get going!
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
